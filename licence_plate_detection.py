# -*- coding: utf-8 -*-
"""

@author: ullu_da_pattha

Created by SKS, HBL Power Systems, Indian Railways
Date - 30/09/2015
"""

import numpy as np
import cv2

licence_cascade = cv2.CascadeClassifier('haarcascade_licence_plate_rus_16stages.xml')

cap = cv2.VideoCapture(0)

while 1:
    ret, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = licence_cascade.detectMultiScale(gray, 1.3, 5)

    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        

    cv2.imshow('img',img)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()