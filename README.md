# README #

Code helps in identifying license plates on vehicles. Once the license plate is identified, we can use the [digit recognition python library](https://bitbucket.org/dronefreak/digit_recognition_python_opencv)

### Running ###

Clone the repo and run the code with the pics provided. You can use custom pics also. Change the pic name accordingly in the code.


### Contribution guidelines ###

* Writing tests
* Code review (Trying to implement in real-time)
* Other guidelines